"use strict";

// Теоретический вопрос

// Опишите своими словами, что такое метод обьекта
// Ответ: Методом объекта называется функция, которяа является свойством объекта.
//        Методом можно добавлять в объекты, преобразовывать, обращатьс к объекту.

// Задание
// Реализовать функцию для создания объекта "пользователь". Задача должна быть реализована на языке javascript,
// без использования фреймворков и сторонник библиотек (типа Jquery).

// Технические требования:
// Написать функцию createNewUser(), которая будет создавать и возвращать объект newUser.
// При вызове функция должна спросить у вызывающего имя и фамилию.
// Используя данные, введенные пользователем, создать объект newUser со свойствами firstName и lastName.
// Добавить в объект newUser метод getLogin(), который будет возвращать первую букву имени пользователя,
// соединенную с фамилией пользователя, все в нижнем регистре (например, Ivan Kravchenko → ikravchenko).
// Создать пользователя с помощью функции createNewUser(). Вызвать у пользователя функцию getLogin().
// Вывести в консоль результат выполнения функции.
// Необязательное задание продвинутой сложности:
// Сделать так, чтобы свойства firstName и lastName нельзя было изменять напрямую.
// Создать функции-сеттеры setFirstName() и setLastName(), которые позволят изменить данные свойства.

function createNewUser() {
  let name = prompt("Input your name");
  let lastName = prompt("Input your last name");
  return {
    _firstName: name,
    _lastName: lastName,
    getLogin: function () {
      return (
        this._firstName.slice(0, 1).toLowerCase() + this._lastName.toLowerCase()
      );
    },
    set firstName(value) {
      this._firstName = value;
    },
    set lastName(value) {
      this._lastName = value;
    },
  };
}
let newUser = createNewUser();
console.log(newUser.getLogin());
