"use strict";

// Теоретический вопрос

// Опишите своими словами, как Вы понимаете, что такое обработчик событий.
// Ответ: Обработчик событий - это функция, которая обрабатывает или откликается на событие,
// которое уже произошло. Благодаря обработчику, код реагирует на действия пользователя.
// Именно браузер вызывает обработчик, когда возникает каке-то событие в каком-нибудь объекте.

// Задание

// Создать поле для ввода цены с валидацией. Задача должна быть реализована
// на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).

// Технические требования:
// При загрузке страницы показать пользователю поле ввода (input) с надписью Price.
// Это поле будет служить для ввода числовых значений
// Поведение поля должно быть следующим:
// При фокусе на поле ввода - у него должна появиться рамка зеленого цвета.
// При потере фокуса она пропадает.
// Когда убран фокус с поля - его значение считывается, над полем создается span,
// в котором должен быть выведен текст: Текущая цена: ${значение из поля ввода}.
// Рядом с ним должна быть кнопка с крестиком (X).
// Значение внутри поля ввода окрашивается в зеленый цвет.
// При нажатии на Х - span с текстом и кнопка X должны быть удалены. Значение,
// введенное в поле ввода, обнуляется.
// Если пользователь ввел число меньше 0 - при потере фокуса подсвечивать поле
// ввода красной рамкой, под полем выводить фразу - Please enter correct price.
// span со значением при этом не создается.
// В папке img лежат примеры реализации поля ввода и создающегося span.

let divspan = document.createElement("div");
document.body.append(divspan);
let div = document.createElement("div");
document.body.append(div);
div.classList.add("alert");
let input = document.createElement("input");
input.placeholder = "PRICE";
div.append(input);
input.addEventListener("focus", function (e) {
  e.target.style.border = "3px solid #00FF00";
});
let correctPrice = document.createElement("span");
divspan.append(correctPrice);
let unconcorrectText = document.createElement("p");
unconcorrectText.innerText = "Please enter correct price";
// unconcorrectText.style.color = "black";
let xBtn = document.createElement("button");
xBtn.innerText = "X";
input.addEventListener("blur", function (e) {
  // e.target.style.border = "3px solid #00FF00";
  // correctPrice.innerHTML = input.value;

  if (input.value < 0 || input.value === "") {
    e.target.style.border = "3px solid #FF0000";
    correctPrice.remove();
    document.querySelector("input").after(unconcorrectText);
    unconcorrectText.append(div);
    // e.target.insertAdjacentHTML("afterEnd", "<br>Please enter correct price");
  } else {
    e.target.style.color = "#00FF00";
    unconcorrectText.remove();
    correctPrice.innerHTML = "ТЕКУЩАЯ ЦЕНА: " + input.value;
    divspan.append(correctPrice);
    divspan.append(xBtn);
  }
});

xBtn.addEventListener("click", function (e) {
  input.value = "";
  xBtn.remove();
  correctPrice.remove();
});
