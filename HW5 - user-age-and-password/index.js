"use strict";

// Теоретический вопрос

// Опишите своими словами, что такое экранирование, и зачем оно нужно в языках программирования
// Ответ: Это метод скрытия от интерпритатора при считывании кода специальных символов (ковычек, обратных слешей)
// или специальных операций (повторы, табуляци новой строки, прямая речть в контексте и.д.), чтобы не возникало ошибки.

// Задание

// Дополнить функцию createNewUser() методами подсчета возраста пользователя и его паролем.
// Задача должна быть реализована на языке javascript, без использования фреймворков и
// сторонник библиотек (типа Jquery).

// Технические требования:
// Возьмите выполненное домашнее задание номер 4 (созданная вами функция createNewUser()) и
// дополните ее следующим функционалом:
// При вызове функция должна спросить у вызывающего дату рождения (текст в формате dd.mm.yyyy) и
// сохранить ее в поле birthday.
// Создать метод getAge() который будет возвращать сколько пользователю лет.
// Создать метод getPassword(), который будет возвращать первую букву имени пользователя в верхнем регистре,
// соединенную с фамилией (в нижнем регистре) и годом рождения.
// (например, Ivan Kravchenko 13.03.1992 → Ikravchenko1992).
// Вывести в консоль результат работы функции createNewUser(), а также функций getAge() и
// getPassword() созданного объекта.

function createNewUser() {
  let name = prompt("Input your name");
  let lastName = prompt("Input your last name");
  let birthDay = prompt("Input the date of your birthday (dd.mm.yyyy)");
  return {
    _firstName: name,
    _lastName: lastName,
    birthdayDate: birthDay,

    getLogin: function () {
      return (
        this._firstName.slice(0, 1).toLowerCase() + this._lastName.toLowerCase()
      );
    },
    getAge: function () {
      let dateTuday = new Date();
      let day = this.birthdayDate.slice(0, 2);
      let mounth = this.birthdayDate.slice(3, 5);
      let years = this.birthdayDate.slice(6);
      let age = dateTuday.getFullYear() - years;
      if (
        mounth > dateTuday.getMonth() ||
        (mounth === dateTuday.getMonth() && day > dateTuday.getDay())
      ) {
        age--;
      }
      return age;
    },
    getPassword: function () {
      return (
        this._firstName[0].toUpperCase() +
        this._lastName.toLowerCase() +
        this.birthdayDate.slice(6)
      );
    },
  };
}

let newUser = createNewUser();

console.log(newUser.getLogin());
console.log(newUser.getAge());
console.log(newUser.getPassword());
